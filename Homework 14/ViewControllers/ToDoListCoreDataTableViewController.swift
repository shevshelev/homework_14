//
//  ToDoListCoreDataTableViewController.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import UIKit

class ToDoListCoreDataTableViewController: UITableViewController {
    
    var taskList: [Task] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        taskList = CoreDataManager.shared.fetchData()
    }
    
    @IBAction func addTask(_ sender: Any) {
        showAlert()
    }
    
    private func saveTask(taskTitle: String) {
        let task = CoreDataManager.shared.createNewTask(with: taskTitle)
        taskList.append(task)
        tableView.insertRows(
            at: [IndexPath(row: taskList.count - 1, section: 0)],
            with: .automatic
        )
    }
}

// MARK: - UITableViewDataSource

extension ToDoListCoreDataTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        taskList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let task = taskList[indexPath.row]
        var content = cell.defaultContentConfiguration()
        content.text = task.title
        cell.contentConfiguration = content
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ToDoListCoreDataTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let task = taskList[indexPath.row]
        showAlert(task: task) {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let task = taskList[indexPath.row]
        if editingStyle == .delete {
            taskList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            CoreDataManager.shared.deleteTask(task: task)
        }
    }
}

// MARK: - AlertController

extension ToDoListCoreDataTableViewController {
    
    private func showAlert(task: Task? = nil, completion: (() -> Void)? = nil) {
        let title = task != nil ? "Update Task" : "New Task"
        let alert = UIAlertController.createTaskAlertController(withTitle: title)
        
        alert.action(task: task, taskTitle: task?.title) { taskTitle in
            if let task = task, let completion = completion {
                task.title = taskTitle
                CoreDataManager.shared.saveContext()
                completion()
            } else {
                self.saveTask(taskTitle: taskTitle)
            }
        }
        present(alert, animated: true)
    }
}
