//
//  ToDoListRealmTableTableViewController.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import RealmSwift

class ToDoListRealmTableViewController: UITableViewController {
    
    var taskList: Results<RealmTask>!

    override func viewDidLoad() {
        super.viewDidLoad()
        taskList = RealmManager.shared.realm.objects(RealmTask.self)
    }
    @IBAction func addTask(_ sender: Any) {
        showAlert()
    }
}

// MARK: - UITableViewDataSource

extension ToDoListRealmTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        taskList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let task = taskList[indexPath.row]
        var content = cell.defaultContentConfiguration()
        content.text = task.title
        cell.contentConfiguration = content
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ToDoListRealmTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let task = taskList[indexPath.row]
        showAlert(task: task) {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let task = taskList[indexPath.row]
        if editingStyle == .delete {
            RealmManager.shared.delete(task)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

// MARK: - AlertController

extension ToDoListRealmTableViewController {
    
    private func showAlert(task: RealmTask? = nil, completion: (() -> Void)? = nil) {
        let title = task != nil ? "Update Task" : "New Task"
        let alert = UIAlertController.createTaskAlertController(withTitle: title)
        
        alert.action(task: task, taskTitle: task?.title) { taskTitle in
            if let task = task, let completion = completion {
                RealmManager.shared.editTask(task, taskTitle)
                completion()
            } else {
                let task = RealmTask(value: [taskTitle])
                RealmManager.shared.save(task)
                self.tableView.insertRows(
                    at: [IndexPath(row: self.taskList.count - 1, section: 0)],
                    with: .automatic
                )
            }
        }
        present(alert, animated: true)
    }
}
