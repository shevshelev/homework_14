//
//  WeatherViewController.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import RealmSwift
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet var weatherImage: UIImageView!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var windLabel: UILabel!
    
    private let locationManager = CLLocationManager()

    private var currentLocation: CLLocation?
    private var weather:Weather?
    private var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaultsManager.shared.lastTimezone != nil {
            weather = RealmManager.shared.fetchWeather()
        }
        setupLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSubview()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let forecastVC = segue.destination as? ForecastTableViewController else {return}
        guard let weatherForecast = weather?.daily else {return}
        forecastVC.weatherForecast = weatherForecast
    }

    private func setSubview() {
        guard let current = weather?.current else {return}
        tempLabel.text = "\(current.temp)ºC"
        windLabel.text = """
                        Wind speed: \(current.wind_speed) m/s
                        Wind direction: \(current.wind_deg)º
                        """
        let imageName = current.weather[0].icon
        weatherImage.image = UIImage(data: Loader.shared.fetchImage(imageName: imageName))
    }
    
    private func setUrlString() {
        guard let lat = currentLocation?.coordinate.latitude else {return}
        guard let lon = currentLocation?.coordinate.longitude else {return}
        urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=\(lat)&units=metric&lon=\(lon)&exclude=alerts&appid=0a8a178a7b51909258dce4751dbc58e5"
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil {
            currentLocation = locations.first
            locationManager.stopUpdatingLocation()
            setUrlString()
            
            Loader.shared.fetchWeather(urlString: urlString) { (result) in
                switch result {
                case .success(let weather):
                    DispatchQueue.main.async {
                        self.weather = weather
                        RealmManager.shared.saveWeather(weather)
                        UserDefaultsManager.shared.lastTimezone = weather.timezone
                        self.setSubview()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func setupLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
}
