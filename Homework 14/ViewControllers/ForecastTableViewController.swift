//
//  ForecastTableViewController.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import RealmSwift

class ForecastTableViewController: UITableViewController {
    
    var weatherForecast = List<Daily>()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 125
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        weatherForecast.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as? WeatherForecastTableViewCell
        let forecast = weatherForecast[indexPath.row]
        cell?.setDescription(forecast: forecast)
        cell?.setImage(forecast: forecast)

        return cell ?? UITableViewCell()
    }
}

