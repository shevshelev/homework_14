//
//  UserDefaultsViewController.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import UIKit

class UserDefaultsViewController: UIViewController {

    @IBOutlet var firstNameTF: UITextField!
    @IBOutlet var lastNameTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTF.text = UserDefaultsManager.shared.firstName
        lastNameTF.text = UserDefaultsManager.shared.lastName
    }
}

extension UserDefaultsViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        switch textField {
        case firstNameTF:
            UserDefaultsManager.shared.firstName = firstNameTF.text
        default:
            UserDefaultsManager.shared.lastName = lastNameTF.text
        }
    }
}
