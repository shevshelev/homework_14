//
//  RealmManager.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import RealmSwift

class RealmManager {
    
    static let shared = RealmManager()
    
    let realm = try! Realm()
    
    func save<T: Object>(_ object: T) {
        write {
            realm.add(object)
        }
    }
    
    func saveWeather(_ weather: Weather) {
        write {
            realm.add(weather, update: .modified)
        }
    }
    
    func editTask(_ task: RealmTask, _ newTitle: String){
        write {
            task.title = newTitle
        }
    }
    
    func delete(_ task: RealmTask) {
        write {
            realm.delete(task)
        }
    }
    
    func fetchWeather() -> Weather? {
        return realm.object(ofType: Weather.self, forPrimaryKey: UserDefaultsManager.shared.lastTimezone)
    }
    
    private init() {}
    
    private func write(completion: () -> Void) {
        do {
            try realm.write {
                completion()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
