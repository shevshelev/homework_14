//
//  CoreDataManager.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import CoreData

class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    private var context: NSManagedObjectContext = {
        let container = NSPersistentContainer(name: "Homework_14")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container.viewContext
    }()

    func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func fetchData() -> [Task] {
        let request = Task.fetchRequest()
        var taskList: [Task] = []
        do {
            taskList = try context.fetch(request)
        } catch {
            print(error.localizedDescription)
        }
        return taskList
    }
    
    func createNewTask(with title: String) -> Task {
        let task = Task(context: context)
        task.title = title
        saveContext()
        return task
    }
    
    func deleteTask(task: Task) {
        context.delete(task)
        saveContext()
    }
    private init() {}
}
