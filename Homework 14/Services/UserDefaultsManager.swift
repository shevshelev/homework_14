//
//  UserDefaultsManager.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import Foundation

class UserDefaultsManager {
    
    static let shared = UserDefaultsManager()
    
    private let kFirstNameKey = "UserDefaultsPersistance.kFirstNameKey"
    private let kLastNameKey = "UserDefaultsPersistance.kLastNameKey"
    private let kLastTimezone = "UserDefaultsPersistance.kLastTimezone"
    
    var firstName: String? {
        set { UserDefaults.standard.set(newValue, forKey: kFirstNameKey)}
        get { return UserDefaults.standard.string(forKey: kFirstNameKey)}
    }
    
    var lastName: String? {
        set { UserDefaults.standard.set(newValue, forKey: kLastNameKey)}
        get { return UserDefaults.standard.string(forKey: kLastNameKey)}
    }
    
    var lastTimezone: String? {
        set { UserDefaults.standard.set(newValue, forKey: kLastTimezone)}
        get { return UserDefaults.standard.string(forKey: kLastTimezone)}
    }
    
    init() {}
}
