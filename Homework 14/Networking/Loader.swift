//
//  Loader.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import Foundation

class Loader {
    
    static let shared = Loader()
    
    func fetchWeather (urlString: String, completion: @escaping (Result <Weather, Error>) -> Void) {
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, let response = response else {
                print(error?.localizedDescription ?? "Some error")
                return
            }
            self.saveDataToCache(with: data, and: response)
                do {
                    let weather =  try JSONDecoder().decode(Weather.self, from: data)
                    completion(.success(weather))
                } catch let jsonError {
                    print("Fail", jsonError.localizedDescription)
                    completion(.failure(jsonError))
                }
        } .resume()
    }
    
    func fetchImage (imageName: String) -> Data {
            let imageURLString = "https://openweathermap.org/img/wn/\(imageName)@2x.png"
            guard let imageUrl = URL(string: imageURLString) else {return Data()}
            guard let imageData = try? Data(contentsOf: imageUrl) else {return Data()}
        return imageData
    }
    
    func saveDataToCache(with data: Data, and response: URLResponse) {
        guard let url = response.url else {return}
        let request = URLRequest(url: url)
        let cachedResponse = CachedURLResponse(response: response, data: data)
        URLCache.shared.storeCachedResponse(cachedResponse, for: request)
    }
    
    func getCachedData(from urlString: String) -> Weather? {
        guard let url = URL(string: urlString) else {return nil}
        var weather: Weather?
        let request = URLRequest(url: url)
        guard let cachedResponse = URLCache.shared.cachedResponse(for: request) else {return nil}
        do {
            let data = try JSONDecoder().decode(Weather.self, from: cachedResponse.data)
            weather = data
        } catch let error {
            print(error)
        }
        return weather
    }
    
    private init() {}
}

