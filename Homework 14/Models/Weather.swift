//
//  Weather.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import RealmSwift

class Weather: Object, Decodable {
    @Persisted(primaryKey: true) var timezone = ""
    @Persisted var daily: List<Daily>
    @Persisted var current: Current?
}

class Current: Object, Decodable {
    @Persisted var dt = 0
    @Persisted var temp = 0.0
    @Persisted var wind_speed = 0.0
    @Persisted var wind_deg = 0
    @Persisted var weather: List<WeatherConditions>
}

class WeatherConditions: Object, Decodable {
    @Persisted var main: String = ""
    @Persisted var icon: String = ""
}

class Daily: Object, Decodable {
    @Persisted var dt = 0
    @Persisted var temp: Temp?
    @Persisted var wind_speed = 0.0
    @Persisted var wind_deg = 0
    @Persisted var weather: List<WeatherConditions>
}

class Temp: Object, Decodable {
    @Persisted var min = 0.0
    @Persisted var max = 0.0
}

