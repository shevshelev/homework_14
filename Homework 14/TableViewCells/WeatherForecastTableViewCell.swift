//
//  WeatherForecastTableViewCell.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import UIKit

class WeatherForecastTableViewCell: UITableViewCell {
    
    @IBOutlet var weatherImage: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    
    func setDescription(forecast: Daily) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        let date = Date(timeIntervalSince1970: Double(forecast.dt))
        let dateString = dateFormatter.string(from: date)
        guard let temp = forecast.temp else {return}
        descriptionLabel.text = """
                                \(dateString)
                                Temp Max: \(temp.max)º
                                Temp Min: \(temp.min)º
                                Wind speed: \(forecast.wind_speed) m/s
                                Wind drection: \(forecast.wind_deg)º
                                """
    }
    
    func setImage(forecast: Daily) {
        let imageName = forecast.weather[0].icon
        DispatchQueue.main.async {
            self.weatherImage.image = UIImage(data: Loader.shared.fetchImage(imageName: imageName))
        }
    }
}
