//
//  AlertController.swift
//  Homework 14
//
//  Created by Shevshelev Lev on 11.12.2021.
//

import UIKit

extension UIAlertController {
    static func createTaskAlertController(withTitle title: String) -> UIAlertController {
        UIAlertController(title: title, message: "What do you want to do?", preferredStyle: .alert)
    }
    
    func action<T>(task: T?, taskTitle: String?, completion: @escaping (String) -> Void) {
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            guard let newTitle = self.textFields?.first?.text else {return}
            guard !newTitle.isEmpty else {return}
            completion(newTitle)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        
        addAction(saveAction)
        addAction(cancelAction)
        addTextField { textField in
            textField.placeholder = "Task"
            textField.text = taskTitle
        }
    }
}
